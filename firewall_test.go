package firewall

import (
	"testing"
)

type testScenario struct {
	Dir   string
	Proto string
	Port  int
	IP    string

	Status bool
}

func TestFirewallLoad(t *testing.T) {
	f, err := LoadFromCSV("fw.csv")
	if err != nil {
		t.Fail()
	}
	if f == nil {
		t.Errorf("Firewall should be initialized")
	}
}

func TestFirewallAcceptPacket(t *testing.T) {
	f, err := LoadFromCSV("fw.csv")
	if err != nil {
		t.Error("Firewall should be initialized")
	}
	var status bool

	tests := []testScenario{
		testScenario{"inbound", "tcp", 80, "192.168.1.2", true},
		testScenario{"inbound", "udp", 53, "192.168.2.1", true},
		testScenario{"outbound", "tcp", 10234, "192.168.10.11", true},
		testScenario{"inbound", "tcp", 81, "192.168.1.2", false},
		testScenario{"inbound", "udp", 24, "52.12.48.92", false},

		testScenario{"inbound", "tcp", 80, "100.168.1.2", true},
		testScenario{"inbound", "tcp", 101, "100.168.1.2", true},
		testScenario{"inbound", "tcp", 80, "100.168.1.3", false},
	}

	for i := range tests {
		status = f.AcceptPacket(tests[i].Dir, tests[i].Proto, tests[i].Port, tests[i].IP)
		if status != tests[i].Status {
			t.Errorf("Expected: %v, Received: %v for %s|%s|%d|%s\n", tests[i].Status, status, tests[i].Dir, tests[i].Proto, tests[i].Port, tests[i].IP)
		}
	}
}
