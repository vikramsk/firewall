# Firewall

Firewall consists of a basic rule engine that determines if a packet should be allowed/disallowed based on the traffic rules provided in the CSV file.

The repository consists of the following:
  - `firewall.go` - The source code for the program.
  - `firewall_test.go` - The tests for the program.
  - `fw.csv` - The CSV file containing the rules for the application.

The implementation converts the IP into an int64 and uses that as the Hashing Key. The idea is to make the lookup operation highly performant. While the CSV load operation can be slower, the AcceptPacket function have a fast response time. 

The AcceptPacket function performs a HashMap lookup using the IP address of the packet. Once it finds that, it performs a lookup on the `<Direction>|<Protocol>` map which points to a sorted array of ports. It then uses a binary search to find the port it's looking for to determine if the packet should be allowed or not.

This implementation is the best I could think of to finish in a 60-90 minute time frame. I'm not very comfortable with loading the IP as the key for the first lookup. I would've liked to build some kind of an interval tree or segment tree for memory efficient lookups and storage, which would've needed some more time to build.

Another area I would've liked to work on is the test cases. The current testing strategy involves creating a table driven test with different scenarios added in sequence. I would like to break it up into separate test functions to improve the readability. 


# Usage

This code doesn't consist of an executable. It's designed to be imported as a package/library and used. The current testing strategy has been to define the rules in the `fw.csv` file and execute the following command:

```sh
go test -v
```

The testing strategy is pretty simple. If the returned status doesn't match the expected, it outputs the key parameters of the received request.

# Illumio Teams

Based on the descriptions provided, the Platform team interests me the most. I have an interest in Distributed Systems and Infrastructure and believe that it would be a good fit for me. I would like to add that when I say Infrastructure, I don't mean DevOps. Basically, I would like to work on building highly scalable services and systems. 

The second team of choice for me would be the Policy Team.

