package firewall

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

// trafficInfo stores the mapping between the traffic type
// which is represented as "<direction>|<protocol>" and
// the eligible ports.
type trafficInfo map[string][]int

// Firewall serves the Firewall API.
type Firewall struct {

	// trafficPortMap maps the ip address
	// with the traffic info object.
	ipTrafficMap map[int64]trafficInfo
}

// LoadFromCSV initializes the Firewall object
// using the CSV rules file.
func LoadFromCSV(csvPath string) (*Firewall, error) {
	if csvPath == "" {
		return nil, fmt.Errorf("firewall: empty file path")
	}

	file, err := os.Open(csvPath)
	if err != nil {
		return nil, fmt.Errorf("firewall: error reading file: %s", err)
	}
	defer file.Close()

	r := csv.NewReader(bufio.NewReader(file))
	records, err := r.ReadAll()
	if err != nil {
		return nil, fmt.Errorf("firewall: unable to parse csv file: %s", err)
	}

	fw := &Firewall{
		ipTrafficMap: make(map[int64]trafficInfo),
	}
	for _, rec := range records {
		dir := rec[0]
		proto := rec[1]

		startPort, endPort := parsePortRange(rec[2])
		startIP, endIP := parseIPRange(rec[3])

		fw.addData(dir, proto, startPort, endPort, startIP, endIP)
	}

	fw.cleanData()
	return fw, nil
}

// FirewallService defines the functionality
// exposed by the Firewall.
type FirewallService interface {
	AcceptPacket(string, string, int, string) bool
}

// AcceptPacket checks if the packet is allowed to pass through the firewall.
// It returns true if it passes the rules, else it returns false.
func (fw *Firewall) AcceptPacket(dir, proto string, port int, ip string) bool {
	ipInt := ipToInt(ip)
	_, ok := fw.ipTrafficMap[ipInt]

	if !ok {
		return false
	}

	ports, ok := fw.ipTrafficMap[ipInt][getKey(dir, proto)]
	if !ok {
		return false
	}

	i := sort.Search(len(ports), func(i int) bool { return ports[i] >= port })
	if i < len(ports) && ports[i] == port {
		return true
	}
	return false
}

func (fw *Firewall) addData(dir, proto string, startPort, endPort int, startIP, endIP int64) {
	for i := startIP; i <= endIP; i++ {
		dirProtoKey := getKey(dir, proto)
		if fw.ipTrafficMap[i] == nil {
			fw.ipTrafficMap[i] = trafficInfo(make(map[string][]int))
			if fw.ipTrafficMap[i][dirProtoKey] == nil {
				fw.ipTrafficMap[i][dirProtoKey] = make([]int, 0)
			}
		}

		for j := startPort; j <= endPort; j++ {
			fw.ipTrafficMap[i][dirProtoKey] = append(fw.ipTrafficMap[i][dirProtoKey], j)
		}
	}
}

func (fw *Firewall) cleanData() {
	for i := range fw.ipTrafficMap {
		for k := range fw.ipTrafficMap[i] {
			sort.Ints(fw.ipTrafficMap[i][k])
		}
	}
}

func getKey(dir, proto string) string {
	return fmt.Sprintf("%s|%s", dir, proto)
}

func parsePortRange(portInfo string) (int, int) {
	var startPort, endPort int
	portTokens := strings.Split(portInfo, "-")
	startPort, _ = strconv.Atoi(portTokens[0])
	endPort, _ = strconv.Atoi(portTokens[0])
	if len(portTokens) == 2 {
		endPort, _ = strconv.Atoi(portTokens[1])
	}

	return startPort, endPort
}

func parseIPRange(ipInfo string) (int64, int64) {
	var startIP, endIP int64
	ipTokens := strings.Split(ipInfo, "-")
	startIP = ipToInt(ipTokens[0])
	if len(ipTokens) == 2 {
		endIP = ipToInt(ipTokens[1])
	} else {
		endIP = startIP
	}

	return startIP, endIP
}

func ipToInt(ip string) int64 {
	octetTokens := strings.Split(ip, ".")

	paddedIPString := ""
	for i := range octetTokens {
		paddedIPString += fmt.Sprintf("%03s", octetTokens[i])
	}

	finalIP, _ := strconv.ParseInt(paddedIPString, 10, 64)
	return finalIP
}
